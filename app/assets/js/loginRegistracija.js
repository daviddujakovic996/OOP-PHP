


    document.getElementById("username").addEventListener("blur", function () {

        var emailJ = document.getElementById("username").value;

        var reEmail = /^([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])+\@[a-z]+\.([a-z]+\.)?[a-z]{2,4}$/;
        if (!reEmail.test(emailJ.trim())) {
            document.getElementById("username").parentElement.style.borderColor = "red"
            document.getElementById("username").style.color = "red"

        } else {
            document.getElementById("username").parentElement.style.borderColor = "green"
            document.getElementById("username").style.color = "green"

        }

    });
    document.getElementById("login").addEventListener("click", function (e) {
        e.preventDefault();
        var emailJ = document.getElementById("username").value;
        var sifraJ = document.getElementById("password").value;

        var reEmail = /^([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])+\@[a-z]+\.([a-z]+\.)?[a-z]{2,4}$/;
        var validno = true;
        if (!reEmail.test(emailJ.trim())) {
            validno = false;
            document.getElementById("username").parentElement.style.borderColor = "red"
        }

        if (sifraJ == "") {
            validno = false;
            document.getElementById("password").parentElement.style.borderColor = "red"

        }
        if (validno) {
            $.ajax({
                url: "index.php?page=login",
                method: "POST",
                dataType: "json",
                data: {
                    email: emailJ,
                    sifra: sifraJ,
                    send: true
                },
                success: function (data,textStatus,xhr) {
                    if (xhr.status == 200) {
                        
                        alert("Upravo ste ulogovani!");
                        var greska = document.getElementById("greska");

                        greska.innerHTML = "";
                        window.location = 'index.php';
                    }
                 
                    
                    
                },
                error: function (xhr, status, error) {
                  
                    if (xhr.status == 409)
                        document.getElementById("greska").innerHTML = "Ne postoji takav korisnik"
                    if (xhr.status == 422)
                        document.getElementById("greska").innerHTML = "Pogresan zahtev"
                    if (xhr.status == 500)
                        document.getElementById("greska").innerHTML = "Serverski problem"
                }
            })
        }
        else {

        }
    })

    document.getElementById("ime").addEventListener("blur", function () {
        var ime = document.getElementById("ime").value;
        var reIme = /^[A-Z��Ȋ�][a-z���]{1,19}(\s[A-Z��Ȋ�][a-z���]{1,19})*$/;
        if (!reIme.test(ime.trim())) {
            document.getElementById("ime").parentElement.style.borderColor = "red"
            document.getElementById("ime").style.color = "red"

        } else {
            document.getElementById("ime").parentElement.style.borderColor = "green"
            document.getElementById("ime").style.color = "green"

        }
    });

    document.getElementById("prezime").addEventListener("blur", function () {
        var prezime = document.getElementById("prezime").value;
        var rePrezime = /^[A-Z��Ȋ�][a-z���]{1,19}(\s[A-Z��Ȋ�][a-z���]{1,19})*$/;
        if (!rePrezime.test(prezime.trim())) {
            document.getElementById("prezime").parentElement.style.borderColor = "red"
            document.getElementById("prezime").style.color = "red"

        } else {
            document.getElementById("prezime").parentElement.style.borderColor = "green"
            document.getElementById("prezime").style.color = "green"

        }
    });

    document.getElementById("emailsignup").addEventListener("blur", function () {
        var email = document.getElementById("emailsignup").value;

        var reEmail = /^([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])+\@[a-z]+\.([a-z]+\.)?[a-z]{2,4}$/;
        if (!reEmail.test(email.trim())) {
            document.getElementById("emailsignup").parentElement.style.borderColor = "red"
            document.getElementById("emailsignup").style.color = "red"

        } else {
            document.getElementById("emailsignup").parentElement.style.borderColor = "green"
            document.getElementById("emailsignup").style.color = "green"

        }

    });

    document.getElementById("passwordsignup").addEventListener("blur", function () {
        var sifra = document.getElementById("passwordsignup").value;
        if (sifra == "") {
            document.getElementById("passwordsignup").parentElement.style.borderColor = "red"
            document.getElementById("passwordsignup").style.color = "red"

        } else {
            document.getElementById("passwordsignup").parentElement.style.borderColor = "green"
            document.getElementById("passwordsignup").style.color = "green"

        }
    });

    document.getElementById("passwordsignup_confirm").addEventListener("blur", function () {
        var sifraPonovo = document.getElementById("passwordsignup_confirm").value;
        var sifra = document.getElementById("passwordsignup").value;
        if (sifra != sifraPonovo || sifraPonovo == "") {
            document.getElementById("passwordsignup_confirm").parentElement.style.borderColor = "red"
            document.getElementById("passwordsignup_confirm").style.color = "red"

        } else {
            document.getElementById("passwordsignup_confirm").parentElement.style.borderColor = "green"
            document.getElementById("passwordsignup_confirm").style.color = "green"

        }
    });

    document.getElementById("registracija").addEventListener("click", function (e) {
        e.preventDefault();
        var ime = document.getElementById("ime").value;
        var prezime = document.getElementById("prezime").value;
        var email = document.getElementById("emailsignup").value;
        var sifra = document.getElementById("passwordsignup").value;
        var sifraPonovo = document.getElementById("passwordsignup_confirm").value;
        var reIme = /^[A-Z��Ȋ�][a-z���]{1,19}(\s[A-Z��Ȋ�][a-z���]{1,19})*$/;
        var rePrezime = /^[A-Z��Ȋ�][a-z���]{1,19}(\s[A-Z��Ȋ�][a-z���]{1,19})*$/;
        var reEmail = /^([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])([A-Z]|[a-z]|[0-9]|[\_\%\.\+\-])+\@[a-z]+\.([a-z]+\.)?[a-z]{2,4}$/;
        var validno = true;
        if (!reIme.test(ime.trim())) {
            validno = false
            document.getElementById("ime").parentElement.style.borderColor = "red"
            document.getElementById("ime").style.color = "red"

        }

        if (!rePrezime.test(prezime.trim())) {
            validno = false
            document.getElementById("prezime").parentElement.style.borderColor = "red"
            document.getElementById("prezime").style.color = "red"

        }

        if (!reEmail.test(email.trim())) {
            validno = false
            document.getElementById("emailsignup").parentElement.style.borderColor = "red"
            document.getElementById("emailsignup").style.color = "red"

        }

        if (sifra == "") {
            validno = false
            document.getElementById("passwordsignup").parentElement.style.borderColor = "red"
            document.getElementById("passwordsignup").style.color = "red"

        } if (sifra != sifraPonovo || sifraPonovo == "") {
            validno = false
            document.getElementById("passwordsignup_confirm").parentElement.style.borderColor = "red"
            document.getElementById("passwordsignup_confirm").style.color = "red"

        }

        if (validno) {
            $.ajax({
                url: "index.php?page=registracija",
                method: "post",
                dataType: "json",
                data: {
                    ime: ime,
                    prezime: prezime,
                    email: email,
                    sifra: sifraPonovo,
                    send: true
                },
                success: function(response,status,data) {
                    
                   // console.log(data.status);
                    if (status == 201) {
                      //  console.log('radi');
                        alert("Upravo si registrovan!");
                        var greska = document.getElementById("greska");

                        greska.innerHTML = "";
                    }
                    
                },
                error: function(data) {
                    if (data.status == 400) {
                        var greska = document.getElementById("greska");
                        greska.style.color = "red";
                        greska.innerHTML = "Ispravite ili popunite polja podvucena crvenom bojom! Ime i prezime moraju poceti velikim slovom!";
                    }
                    
                }
            })
        }
        else {
            var greska = document.getElementById("greska");
            greska.style.color = "red";
            greska.innerHTML = "Ispravite ili popunite polja podvucena crvenom bojom! Ime i prezime moraju poceti velikim slovom!";
        }
    })

