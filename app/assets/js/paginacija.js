window.onload = function () {
    

    $.ajax({
        url: "index.php?page=paginacija",
        method: "GET",
        dataType: "json",
        data: {
            send: true,

        },
        success: function (data) {         
            popuniPodatke(data);
            prikaziPaginaciju(data[data.length - 1]);

        },
        error: function (xhr, status, error) {
            
            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takva vest"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
    })

   

    function popuniPodatke(podaci) {
        let ispis = "";
        for (let v of podaci) {
            if (v.IdPost == undefined) {
                break;
            }
            ispis += `

            <div class="single-recent-blog-post">
              <div class="thumb">
                <img class="img-fluid" src="app/assets/img/${v.Putanja}" alt="${v.NazivSlike}">
                <ul class="thumb-info">
                  <li><a href="#"><i class="ti-user"></i>${v.Ime}</a></li>
                  <li><a href="#"><i class="ti-notepad"></i>${v.DatumObjave}</a></li>
                  <li><a href="#"><i class="fa fa-eye"></i>${v.BrojPregleda}</a></li>
                </ul>
              </div>
              <div class="details mt-20">
                <a href="index.php?page=SinglePage&id=${v.IdPost}">
                  <h3>${v.NazivPosta}</h3>
                </a>
                <p class="tag-list-inline">Kategorija: <a href="#">${v.Naziv}</a></p>
                <p>${v.Opis}</p>
                <a class="button" href="index.php?page=SinglePage&id=${v.IdPost}">Detaljnije.. <i class="ti-arrow-right"></i></a>
              </div>
            </div>
		
 
`
        }
        if (ispis == "") {
            ispis += "<h1 style='text-align:center'>TRENUTNO NEMA VESTI U TOJ KATEGORIJI!</h1>";


        }

        document.getElementsByClassName("prikaz")[0].innerHTML= ispis;

    }
   
    function prikaziPaginaciju(brojProizvoda) {
        
        let brojPoStrani=4;
        let ukupnoStranica=brojProizvoda/brojPoStrani;

        let ispis="<li class='page-item'> <a href='#' class='page-link' aria-label='Previous' data-id='1'><span aria-hidden='true'><i class='ti-angle-left'></i></span></a></li>";
        for (let i=1; i<ukupnoStranica+1; i++) {

            ispis+="<li class='page-item'><a href='#' class='page-link' data-id='" + i + "'>" + i + " </a></li>";
        }

        ispis +="<li class='page-item'><a href='#' class='page-link' aria-label='Next' data-id='"+Math.round(ukupnoStranica)+"'><span aria-hidden='true'><i class='ti-angle-right'></i></span></a ></li>";
        
       document.getElementsByClassName("pagination")[0].innerHTML= ispis;
                                           
       
        
        $(".page-link").click(function (e) {

            e.preventDefault();

            var pagin = $(this).data("id");
          
            var paginUInt = parseInt(pagin);
            
            $.ajax({
                url: "index.php?page=paginacija",
                method: "GET",
                data: { send: true, strana: paginUInt },
                dataType: "json",
                success: function (data) {
                    
                    popuniPodatke(data);

                },
                error: function (xhr, status, error) {
                    alert("greska");
                    if (xhr.status == 409)
                        document.getElementById("greska").innerHTML = "Ne postoji takva vest"
                    if (xhr.status == 422)
                        document.getElementById("greska").innerHTML = "Pogresan format"
                    if (xhr.status == 500)
                        document.getElementById("greska").innerHTML = "Serverski problem"
                }
            });
        })
    }


    $(".kategorija").click(function (e) {
        e.preventDefault();
        var kategorija = $(this).data("id");
        // var paginUInt = parseInt(kategorija);

        $.ajax({
            url: "index.php?page=kategorija",
            method: "post",
            data: { send: true, kategorija: kategorija },
            dataType: "json",
            success: function (data) {
                
                popuniPodatke(data);
                
                //prikaziPaginaciju(data[data.length - 1]);
                $(".page-link").css("visibility", "hidden");

            },
            error: function (xhr, status, error) {
                alert("greska");
                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takva vest"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        });




    });
   
    

    $(".prikaziSve").click(function (e) {

        e.preventDefault;
        $.ajax({
            url: "index.php?page=paginacija",
            method: "GET",
            dataType: "json",
            data: {
                send: true,

            },
            success: function (data) {
                popuniPodatke(data);
                prikaziPaginaciju(data[data.length - 1]);

            },
            error: function (xhr, status, error) {

                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takva vest"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        })
    })
   
    
}
  