//window.onload = function () {
  //  e.preventDefault;

   
    document.getElementById("klik").addEventListener("click", function (e) {
        e.preventDefault();
        var naslovN = document.getElementById("subject").value;

        var komentarN = document.getElementById("message").value;

        var korisnik = document.getElementById("idKorisnik").value;

        var vest = document.getElementById("idPosta").value;

        var validno = true;


        if (naslovN == "") {
            document.getElementById("subject").style.borderColor = "red";
            validno = false;
        }
        if (komentarN == "") {
            $("#message").css("border-color", "red");
            validno = false;
        }
        if (validno) {

            $.ajax({
                url: "index.php?page=komentari",
                method: "post",
                data: { send: true, naslov: naslovN, komentar: komentarN, korisnik: korisnik, vest: vest },
                dataType: "json",
                success: function (data) {
                    //alert(data);
                    alert("uspesno unesen komentar");
                    //alert("nesto");
                    prikazKomentara(data);

                },
                error: function (xhr, status, error) {
                    alert("greska pri unosu komentara");
                    if (xhr.status == 409)
                        document.getElementById("greska").innerHTML = "Ne postoji takva vest"
                    if (xhr.status == 422)
                        document.getElementById("greska").innerHTML = "Pogresan format"
                    if (xhr.status == 500)
                        document.getElementById("greska").innerHTML = "Serverski problem"
                }

            })
            // return true;
        } else {
            alert("ne valja");
            //return false;
        }
    })

    
    function prikazKomentara(data) {
        let ispis = "";
        //alert(data);
        for (let v of data) {
            if (v.IdKomentar == undefined) {
                break;
            }
            ispis += `

              <div class="comment-list">
                        <div class="single-comment justify-content-between d-flex">
                            <div class="user justify-content-between d-flex">
                                
                                <div class="desc">
                                    <h5><a href="#">${v.Naslov}</a></h5>
                                    <p class="date">${v.Ime} ${v.Prezime}</p>
                                    <p class="comment">
                                       ${v.Komentar}
                                    </p>
                                </div>
                            </div>
                           
                        </div>
                    </div>	
 
`
        }
        if (ispis == "") {
            ispis += "<h1 style='text-align:center'>TRENUTNO NEMA VESTI U TOJ KATEGORIJI!</h1>";


        }

        document.getElementsByClassName("komentari")[0].innerHTML = ispis;
    }
//}


