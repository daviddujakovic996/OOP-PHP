function proveraMejla() {
    
	var naslovJ=document.getElementById("subject").value;
	var emailJ=document.getElementById("email").value;
    var komentarJ = document.getElementById("message").value;
  
	var validno=true;
	
	var reNaslov=/^[A-ZČĆŠĐŽ][a-zšđžćč]{1,49}(\s[a-zšđžćč]{1,49})*$/;
	var reEmail=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	

    if (naslovJ == "") {
		validno=false;
		document.getElementById("subject").style.borderColor="red";
	}
	if(!reEmail.test(emailJ.trim())){
		validno=false;
		document.getElementById("email").style.borderColor="red";
	}
	if(komentarJ==""){
		validno=false;
		document.getElementById("message").style.borderColor="red";
	}
	
	if(validno){
		$.ajax({
			url:"index.php?page=contact",
			method:"post",
			type:"json",
			data:{
				subject:naslovJ,
				email:emailJ,
                message: komentarJ,
                send:true
			},
            success: function (data) {
                console.log(data);
                    alert("Uspešno ste poslali mejl , ogovorićemo Vam u što kraćem roku!");
                    //window.location('')
                
				
				//window.location.href="index.php";
			},
			error:function(){
				alert("NEUSPESNO");
			}
			
		})
		return true;
	}else{
		var greska=document.getElementById("greske");
		greska.innerHTML="<h3 style='text-align:center;'>Ispravite greške koje su uokvirene crvenom bojom!</h3>";
		return false;
	}
	
}