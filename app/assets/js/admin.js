window.onload = function () {


    $.ajax({
        url: "index.php?page=paginacija",
        method: "GET",
        dataType: "json",
        data: {
            send: true,

        },
        success: function (data) {
            popuniPodatke(data);
            prikaziPaginaciju(data[data.length - 1]);

        },
        error: function (xhr, status, error) {

            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takva vest"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
    })





    function prikaziPaginaciju(brojProizvoda) {

        let brojPoStrani = 4;
        let ukupnoStranica = brojProizvoda / brojPoStrani;

        let ispis = "<li class='page-item'> <a href='#' class='page-link' aria-label='Previous' data-id='1'><span aria-hidden='true'><i class='ti-angle-left'></i></span></a></li>";
        for (let i = 1; i < ukupnoStranica + 1; i++) {

            ispis += "<li class='page-item'><a href='#' class='page-link' data-id='" + i + "'>" + i + " </a></li>";
        }

        ispis += "<li class='page-item'><a href='#' class='page-link' aria-label='Next' data-id='" + Math.round(ukupnoStranica) + "'><span aria-hidden='true'><i class='ti-angle-right'></i></span></a ></li>";

        document.getElementsByClassName("pagination")[0].innerHTML = ispis;



        $(".page-link").click(function (e) {

            e.preventDefault();

            var pagin = $(this).data("id");

            var paginUInt = parseInt(pagin);

            $.ajax({
                url: "index.php?page=paginacija",
                method: "GET",
                data: { send: true, strana: paginUInt },
                dataType: "json",
                success: function (data) {

                    popuniPodatke(data);

                },
                error: function (xhr, status, error) {
                    alert("greska");
                    if (xhr.status == 409)
                        document.getElementById("greska").innerHTML = "Ne postoji takva vest"
                    if (xhr.status == 422)
                        document.getElementById("greska").innerHTML = "Pogresan format"
                    if (xhr.status == 500)
                        document.getElementById("greska").innerHTML = "Serverski problem"
                }
            });
        })
    }

    document.getElementById("izmeni").addEventListener("click", function (e) {
        e.preventDefault();
        var naziv = document.getElementById("naziv").value;
        var opis = document.getElementById("opis").value;
        var kategorija = document.getElementById("kategorija");
        var kat = kategorija.options[kategorija.options.selectedIndex].value;
        var idPost = document.getElementById("idPost").value;
      
        $.ajax({
            url: "index.php?page=izmeniPost",
            method: "POST",
            data: { send: true, naziv:naziv, opis:opis, kat:kat, idPost:idPost},
            dataType: "json",
            success: function (data) {
                alert("uspesno");
                popuniPodatke(data);
                document.getElementById("idPost").value = "";
                document.getElementById("naziv").value = "";
                document.getElementById("opis").value = "";
                document.getElementById("detaljno").value = "";
                document.getElementsByTagName("option")[0].innerHTML = "";
                document.getElementsByTagName("option")[0].value = "";
            },
            error: function (xhr, status, error) {
                alert("greska");
                if (xhr.status == 409)
                    document.getElementById("greska").innerHTML = "Ne postoji takva vest"
                if (xhr.status == 422)
                    document.getElementById("greska").innerHTML = "Pogresan format"
                if (xhr.status == 500)
                    document.getElementById("greska").innerHTML = "Serverski problem"
            }
        });
    })
}


function izmeni(id) {
    var idPost = id;
  
    $.ajax({
        url: "index.php?page=izmeniPostForma",
        method: "GET",
        dataType: "json",
        data: {
            send: true,
            idPost: idPost
        },
        success: function (data) {
            popuniFormu(data);
           

        },
        error: function (xhr, status, error) {

            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takva vest"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
    })

}

function popuniFormu(data) {
    
    for (let v of data) {
        document.getElementById("idPost").value = v.IdPost;
        document.getElementById("naziv").value = v.NazivPosta;
        document.getElementById("opis").value = v.Opis;
        document.getElementById("detaljno").value = v.DetaljnoOPostu;
        document.getElementsByTagName("option")[0].innerHTML = v.Naziv;
        document.getElementsByTagName("option")[0].value = v.IdKategorije;




    }
}

function obrisi(id) {
    var idPost = id;
    
    $.ajax({
        url: "index.php?page=obrisiPost",
        method: "POST",
        dataType: "json",
        data: {
            send: true,
            idPost: idPost
        },
        success: function (data) {
            popuniPodatke(data);


        },
        error: function (xhr, status, error) {

            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takva vest"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
    })
}

function popuniPodatke(podaci) {
    let ispis = " <table border='1'>  <tr>      <th>Izmeni</th>   <th>Obrisi</th>   <th>IdPost</th>   <th>Naziv</th>    <th>Opis</th>    <th>Datum</th>    <th>Pregledi</th>    <th>Autor</th>         <th>Kategorija</th>   <th>Slika</th>	</tr>";
    for (let v of podaci) {
        if (v.IdPost == undefined) {
            break;
        }
        ispis += `
               
				<tr>	
							<td class='ellipsis'><button onclick='izmeni(${v.IdPost})' type='button'  class='izmeni'>Izmeni</button></td>
							<td class='ellipsis'><button onclick='obrisi(${v.IdPost})' type='button'  class='obrisi'>Obrisi</button></td>
							<td class='ellipsis'>${v.IdPost}</td>
							<td class='ellipsis'>${v.NazivPosta}</td>
							<td class='ellipsis'>${v.Opis}</td>
							<td class='ellipsis'>${v.DatumObjave}</td>
							<td class='ellipsis'>${v.BrojPregleda}</td>
							<td class='ellipsis'>${v.Ime}</td>
							<td class='ellipsis'>${v.Naziv}</td>				
							<td class='ellipsis'><img width='40px' src="app/assets/img/${v.Putanja}"/></td>	
							</tr>
					
					
`
    }
    ispis += "</table><br/><br/>";

    document.getElementsByClassName("prikaz")[0].innerHTML = ispis;

}

function obrisiKorisnika(id) {
    var idKorisnik = id;
 
    $.ajax({
        url: "index.php?page=obrisiKorisnika",
        method: "POST",
        dataType: "json",
        data: {
            send: true,
            idKorisnik: idKorisnik
        },
        success: function (data) {
            popuniKorisnike(data);


        },
        error: function (xhr, status, error) {

            if (xhr.status == 409)
                document.getElementById("greska").innerHTML = "Ne postoji takva vest"
            if (xhr.status == 422)
                document.getElementById("greska").innerHTML = "Pogresan format"
            if (xhr.status == 500)
                document.getElementById("greska").innerHTML = "Serverski problem"
        }
    })
}

function popuniKorisnike(data) {
    let ispis = "<table border='1'>  <tr><th>Obrisi</th>   <th>IdKorisnik</th>   <th>Ime</th>    <th>Prezime</th>    <th>Emaillllllll</th></tr>";
    for (let v of data) {
        if (v.IdKorisnik == undefined) {
            break;
        }
        ispis += `
               <tr>	
						
							<td class='ellipsis'><button onclick='obrisiKorisnika(${v.IdKorisnik})' type='button'  class='obrisi'>Obrisi</button></td>
							<td class='ellipsis'>${v.IdKorisnik}</td>
							<td class='ellipsis'>${v.Ime}</td>
							<td class='ellipsis'>${v.Prezime}</td>
							<td class='ellipsis'>${v.Email}</td>
						</tr>
				
					
					
`
    }
    ispis += "</table><br/><br/>";

    document.getElementsByClassName("korisnici")[0].innerHTML = ispis;
}