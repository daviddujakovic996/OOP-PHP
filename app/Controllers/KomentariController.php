<?php

namespace App\Controllers;
use App\Models\Komentar;


class KomentariController extends Controller{
     private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function unesiKomentar(){
        if(isset($_POST['send'])){
            $naslov = $_POST['naslov'];
            $komentar = $_POST['komentar'];
			$korisnik = $_POST['korisnik'];
			$vest = $_POST['vest'];
			//$uIntKorisnik=settype($korisnik,"integer");
			//$uIntVest=settype($vest,"integer");

            $kom = new Komentar($this->db);
            $rezultat = $kom->insertUser($naslov,$komentar,$korisnik,$vest);
			if($rezultat){
				$komm = new Komentar($this->db);
				$rezultatt = $komm->dohvatiKomentare($vest);
				echo \json_encode($rezultatt);
				//http_response_code(200);
			}else{
				http_response_code(404);
			}
			
          
        } else {
            \http_response_code(404);
        }
       
    }

	public function prikaziKomentare(){
		if(isset($_POST['send'])){
			$vest = $_POST['vest'];
			$kom = new Komentar($this->db);
            $rezultat = $kom->dohvatiKomentare($vest);
			

			echo \json_encode($rezultat);
          
        } else {
            \http_response_code(404);
        }
	}
}