<?php

namespace App\Controllers;
use App\Models\Admin;


class AdminController extends Controller{
     private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function formaIzmeni(){
        if(isset($_GET["send"])){
			$idPost=$_GET["idPost"];
					
			 
            $vest = new Admin($this->db);
            $rezultat = $vest->getVest($idPost);
		
			echo \json_encode($rezultat);
					
		} else {
            \http_response_code(404);
        }		
    }
	public function izmeniPost(){
        if(isset($_POST["send"])){
			$idPost=$_POST["idPost"];
			$naziv=$_POST["naziv"];
			$opis=$_POST["opis"];
			$kat=$_POST["kat"];
			 
            $vest = new Admin($this->db);
            $rezultat = $vest->izmeniVest($idPost,$naziv,$opis,$kat);

			if($rezultat){
			$sveVesti=$vest->getAll();
			echo \json_encode($sveVesti);
			}
			
					
		} else {
            \http_response_code(404);
        }		
    }

	public function unesiPost(){
		$naziv=$_POST["naziv"];
		$opis=$_POST["opis"];
		$detaljnoOPostu=$_POST["detaljno"];
		$kategorija=$_POST["Kategorija"];
		$slike=$_POST["slika"];
						
		$validno=true;
						
		if(empty($naziv) OR empty($opis) OR empty($detaljnoOPostu)){
			$validno=false;
		}
		if($validno){
		
		$vest = new Admin($this->db);
        $rezultat = $vest->unesiVest($naziv,$opis,$detaljnoOPostu,$kategorija);
		}else{
		 header('Location:index.php?page=admin');
		}
						$slike=array();
						if($rezultat){
						for($i=0;$i<count($_FILES["slika"]["name"]);$i++){
							$fileName = $_FILES['slika']['name'][$i];
							 $tmpName = $_FILES['slika']['tmp_name'][$i];
							 $fileSize = $_FILES['slika']['size'][$i];
							 $fileType = $_FILES['slika']['type'][$i];
							 $fileError = $_FILES['slika']['error'][$i]; 
							 
							 $filepath="app/assets/img/".$fileName;
							 
							 move_uploaded_file($tmpName,$filepath);
							 $img = new Admin($this->db);
							$rez = $img->unesiSliku($fileName);
							 header('Location:index.php?page=admin');
							 
						}
						}
	}

	public function obrisiPost(){
		if(isset($_POST["send"])){
			$idPost=$_POST["idPost"];
			
			$vest = new Admin($this->db);
            $rezultat = $vest->obrisiVest($idPost);

			if($rezultat){
			$sveVesti=$vest->getAll();
			echo \json_encode($sveVesti);
			}
			
					
		} else {
            \http_response_code(404);
        }		
		
	}

	public function obrisiKorisnika(){
		if(isset($_POST["send"])){
			$idKorisnik=$_POST["idKorisnik"];
			
			$kor = new Admin($this->db);
            $rezultat = $kor->obrisiKorisnika($idKorisnik);

			if($rezultat){
			$sviKorisnici=$kor->getAllUser();
			echo \json_encode($sviKorisnici);
			}
			
					
		} else {
            \http_response_code(404);
        }		
		
	}
}