<?php

namespace App\Controllers;
use App\Models\Korisnik;


class LoginController extends Controller{
     private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function ulogujSe(){
        if(isset($_POST['send'])){
            $email = $_POST['email'];
            $password = $_POST['sifra'];
			$pass=md5($password);

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
                $_SESSION['errors']= "Nije email ok!";
            }
            else {
                $korisnik = new Korisnik($this->db);
                $user = $korisnik->getUser($email, $pass);
				
                if($user){
                    $_SESSION['korisnik'] = $user;
					http_response_code(200);
					echo \json_encode($user);
                } else {
					
                    http_response_code(409);
                }
				
            }
			//echo \json_encode($user);
           // header("Location: index.php");
			//return $_SESSION["korisnik"];
        } else {
            http_response_code(404);
        }
       
    }
}