<?php

namespace App\Controllers;

class Controller {

    protected function view($fileName, $data = []){
        // [ "proizvodi", "title"]
        
		

        extract($data); // OD ASOCIJATIVNOG NIZA - PRAVI PROMENJIVE
        //$danijela = 5;
        
        include "app/views/head.php";
        include "app/views/nav.php";
        include "app/views/pages/$fileName.php";
        include "app/views/footer.php";
    }
}