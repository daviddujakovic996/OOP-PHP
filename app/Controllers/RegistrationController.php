<?php

namespace App\Controllers;
use App\Models\Korisnik;


class RegistrationController extends Controller{
     private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function registracija(){
        if(isset($_POST["send"])){
  
		$ime = $_POST["ime"];
		$prezime = $_POST["prezime"];
		$email = $_POST["email"];
		$sifra = $_POST["sifra"];
		$siframd5 = md5($sifra);
		$reIme =  "/^[A-Z��Ȋ�][a-z���]{1,19}(\s[A-Z��Ȋ�][a-z���]{1,19})*$/";
		$rePrezime =  "/^[A-Z��Ȋ�][a-z���]{1,19}(\s[A-Z��Ȋ�][a-z���]{1,19})*$/";
		$validno = true;
		if(!preg_match($reIme, $ime)){
			$validno = false;
		}
		if(!preg_match($rePrezime, $prezime)){
			$validno = false;
		}
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$validno = false; 
		}
		if(empty($sifra)){
			$validno = false;
		}
		if($validno){
			$korisnik = new Korisnik($this->db);
            $user = $korisnik->insertUser($ime, $prezime,$email,$siframd5);
			
           if($user){
		  
		   	   http_response_code(201);
			   echo \json_encode($user);
		   }
		   else{
		     http_response_code(400);
		   }

                
            
			
			
		
	}


            
        } else {
            http_response_code(404);
        }
       
    }
}