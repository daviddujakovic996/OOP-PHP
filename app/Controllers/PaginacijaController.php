<?php

namespace App\Controllers;

use App\Models\Vesti;

class PaginacijaController extends Controller {
    private $db;
	public function __construct($db) {
        $this->db = $db;
		
    }

	public function prikazi(){
		 $vestiModel = new Vesti($this->db);
        $brojVesti = $vestiModel->getAll();
		if(isset($_GET["send"])){
			$brojPoStrani=4;
				isset($_GET["strana"]) ? $strana=$_GET["strana"] : $strana=0;					
						if($strana>0){
							$start=($strana*$brojPoStrani)-$brojPoStrani;
							$vestiPagin=$vestiModel->getFourWithPagin($start,$brojPoStrani);
						}else{
							$vestiPagin=$vestiModel->getFourWithPagin(0,4);
						}
			
				 array_push($vestiPagin,$brojVesti);
				echo \json_encode($vestiPagin);
			}
			}
}




?>
   