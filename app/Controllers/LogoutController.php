<?php

namespace App\Controllers;



class LogoutController extends Controller{
     private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function logout(){
	
		 unset($_SESSION['korisnik']);
		 session_destroy();
		header("Location: index.php");
         
	}
}