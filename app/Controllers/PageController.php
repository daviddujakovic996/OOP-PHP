<?php

namespace App\Controllers;
use App\Models\Kategorije;
use App\Models\Menu;
use App\Models\Vesti;
use App\Models\Slike;
use App\Models\Komentar;
use App\Models\Korisnik;

class PageController extends Controller {
    private $db;


    public function __construct($db) {
        $this->db = $db;
    }
	
	public function menu(){
		$menuModel = new Menu($this->db);
		$menu = $menuModel->getAll();
		return $menu;
	}

    public function home(){
      
       $kategorijeModel = new Kategorije($this->db);
       $kategorije = $kategorijeModel->getAll();
   	
	   $vestiModel = new Vesti($this->db);
       $vestiRez = $vestiModel->getFour();
	   $topTri=$vestiModel->topTri();

	   $nav=$this->menu();
		
       $this->view("home", [
            "title" => "VESNIK",
            "kategorije" => $kategorije,
			"menu"=>$nav,
			"vesti" => $vestiRez,
			"topTri"=>$topTri
        ]);
    }

    public function contact() {
		 $nav=$this->menu();
        $this->view("contact",[
			"title" => "KONTAKT",
			"menu"=>$nav,
		]);
    }

    public function about(){
	 $vestiModel = new Vesti($this->db);
       
	   $topTri=$vestiModel->topTri();
         $nav=$this->menu();
        $this->view("about",[
			"title" => "O NAMA",
			"menu"=>$nav,
			"topTri"=>$topTri
		]);
    }
	 public function singlePage($reguest){
		if(isset($reguest)){
			$vest=$reguest["id"];
			$vestiModel = new Vesti($this->db);
       
			$topTri=$vestiModel->topTri();
			$konkretnaVest=$vestiModel->getVest($vest);
			$brojPregleda=$konkretnaVest[0]->BrojPregleda;

			$podesiBrojPregleda=$vestiModel->podesibp($brojPregleda,$vest);

			$slikeModel=new Slike($this->db);
			$dohvatiSlike=$slikeModel->getImageWithParams($vest);
			$nav=$this->menu();

			$komm = new Komentar($this->db);
			$rezultatt = $komm->dohvatiKomentare($vest);

			$this->view("singlePage",[
				"title" => "VEST",
				"menu"=>$nav,
				"topTri"=>$topTri,
				"vest"=>$konkretnaVest,
				"slike"=>$dohvatiSlike,
				"komentari"=>$rezultatt
			]);
		}else{
		$this->view("singlePage",[
			"title" => "NEMA VESTI",
			
		]);
		}
    }
	public function admin(){
        $nav=$this->menu();
	
		$kategorijeModel = new Kategorije($this->db);
       $kategorije = $kategorijeModel->getAll();
   	
	   $vestiModel = new Vesti($this->db);
       $vestiRez = $vestiModel->getFour();

	   $korisnikModel=new Korisnik($this->db);
	   $korisnici=$korisnikModel->getAll();

        $this->view("admin",[
			"title" => "ADMIN",
			"menu"=>$nav,
			"vesti" => $vestiRez,
			"kategorije" => $kategorije,
			"korisnici"=>$korisnici
		]);
}
	
}
