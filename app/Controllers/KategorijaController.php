<?php

namespace App\Controllers;
use App\Models\Kategorije;


class KategorijaController extends Controller{
     private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function prikaziKategorije(){
        if(isset($_POST['send'])){
            $kategorija = $_POST['kategorija'];
            
            $kat = new Kategorije($this->db);
            $rezultat = $kat->getKategorije($kategorija);
			$broj=$kat->prebroj($kategorija);
			array_push($rezultat,$broj);
			echo \json_encode($rezultat);
          
        } else {
            \http_response_code(404);
        }
       
    }
}