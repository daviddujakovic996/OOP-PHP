<main class="site-main">
    <!--================Hero Banner start =================-->  
    <section class="mb-30px">
      <div class="container">
        <div class="hero-banner">
          <div class="hero-banner__content">
            <h3 style="color:black">VEST</h3>
            <h1 style="color:black">Vise o toj vesti</h1>
            
          </div>
        </div>
      </div>
    </section>
    <!--================Hero Banner end =================-->  

   
    <!--================ Start Blog Post Area =================-->
    <section class="blog-post-area section-margin mt-4">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
			
            <div class="main_blog_details">
			<?php for($i=0;$i<count($vest);$i++){ ?>
				<?php foreach($slike as $rk){ ?>
                <img class="img-fluid" src="app/assets/img/<?= $rk->Putanja; ?>" alt="<?= $rk->NazivSlike; ?>">
				<?php  } ?>
				
				
                <a href="#"><h4><?= $vest[$i]->NazivPosta; ?></h4></a>
                <div class="user_details">
                  <div class="float-left">
                    <a href="#"><?= $vest[$i]->Naziv; ?></a>
                  
                  </div>
                  <div class="float-right mt-sm-0 mt-3">
                    <div class="media">
                      <div class="media-body">
                        <h5><?= $vest[$i]->Ime; ?></h5>
                        <p><?= $vest[$i]->DatumObjave; ?></p>
                      </div>
                      
                    </div>
                  </div>
                </div>
                <p><?= $vest[$i]->DetaljnoOPostu; ?></p>
               <div class="news_d_footer flex-column flex-sm-row">
                 
           <?php }; ?>
			 
			
                 <div class="news_socail ml-sm-auto mt-sm-0 mt-2">
               <a href="#"><i class="fab fa-facebook-f"></i></a>
               <a href="#"><i class="fab fa-twitter"></i></a>
               <a href="#"><i class="fab fa-dribbble"></i></a>
               <a href="#"><i class="fab fa-behance"></i></a>
             </div>
               </div>
              </div>
          

          <div class="navigation-area">
                  <div class="row">
                      <div class="col-lg-6 col-md-6 col-12 nav-left flex-row d-flex justify-content-start align-items-center">
                          
                      </div>
                      <div class="col-lg-6 col-md-6 col-12 nav-right flex-row d-flex justify-content-end align-items-center">
                          
                      </div>									
                  </div>
                </div>

                
				<div class="comments-area">
                    <div class="komentari">
					<?php foreach($komentari as $data){ ?>
					<div class="comment-list">
                        <div class="single-comment justify-content-between d-flex">
                            <div class="user justify-content-between d-flex">
                                
                                <div class="desc">
                                    <h5><a href="#"><?= $data->Naslov ?></a></h5>
                                    <p class="date"><?= $data->Ime . " " . $data->Prezime ?></p>
                                    <p class="comment">
                                       <?= $data->Komentar ?>
                                    </p>
                                </div>
                            </div>
                           
                        </div>
                    </div>	 
					<?php } ?>
					</div>
                </div>
				<input type="hidden" id="idPosta"  value="<?php echo $_GET["id"];?>"/>
                <div class="comment-form">
                    
					<?php if(isset($_SESSION["korisnik"])){ ?><h4>Ostavi komentar</h4>
                    <form method="post" action="<?php $_SERVER['PHP_SELF'] ?>">
						
						<input type="hidden" id="idKorisnik"  value="<?php echo $_SESSION["korisnik"][0]->IdKorisnik;?>"/>
                        <div class="form-group form-inline">
                          <div class="form-group col-lg-6 col-md-6 name">
                            <input type="text" value=" <?php echo $_SESSION["korisnik"][0]->Ime . " " . $_SESSION["korisnik"][0]->Prezime; ?>" class="form-control" id="name" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'" disabled>
                          </div>
                          									
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="subject" placeholder="Subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Subject'">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control mb-10" rows="5" id="message" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                        </div>
                       
						<button type="submit" class="button button-block" id="klik"/>Postavi Komentar</button>
                    </form>
					<?php }else{
					echo "<hr/><br/><h1>MORATE BITI ULOGOVANI DA BI OSTAVILI KOMENTAR</h1><hr/><input type='hidden' id='klik'/>";
					}
					?>
                </div>
        
		  </div><!-- Start Blog Post Siddebar -->
		
          <div class="col-lg-4 sidebar-widgets">
              <div class="widget-wrap">
                <div class="single-sidebar-widget newsletter-widget"><div id="greska"></div>
				<?php if(isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"][0]->IdUl=="2"){
				?>
				<h1>Ime i prezime : <?php echo $_SESSION["korisnik"][0]->Ime . " " . $_SESSION["korisnik"][0]->Prezime; ?><h1>
			<h2>EMAIL: <?php echo $_SESSION["korisnik"][0]->Email; ?></h2>
			<form action="index.php?page=logout" method="post">
               </br> <input type="submit" name="odjava" value="ODJAVI SE" class="btn btn-primary ">
			   
			   </div>
             </form>

				<?php
				}elseif(isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"][0]->IdUl=="1"){
				echo "ZDRAVO ADMINE!<br/><a href='index.php?page=admin'>Admin panel</a>";
				?> 
				<form action="index.php?page=logout" method="post">
               </br> <input type="submit" name="odjava" value="ODJAVI SE" class="btn btn-primary "></div>
             </form>
				<?php
				}else{
				?>
                 <div class="form">
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Registracija</a></li>
        <li class="tab"><a href="#login">Logovanje</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Registruj se besplatno</h1>
          
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Ime<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" id="ime"/>
            </div>
        
            <div class="field-wrap">
              <label>
                Prezime<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" id="prezime"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" id="emailsignup"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id="passwordsignup"/>
          </div>
          
		  <div class="field-wrap">
            <label>
              Ponovi Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id="passwordsignup_confirm"/>
          </div>

          <button type="submit" class="button button-block" id="registracija"/>Registruj se</button>
          
          </form>

        </div>
        
        <div id="login">   
          <h1>Dobrodosli nazad!</h1>
          
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
		
            <div class="field-wrap">
            <label>
              Email<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" id="username"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id="password"/>
          </div>
          
        
          
          <button class="button button-block" id="login"/>Uloguj se</button>
          
          </form>
		 <script src="app/assets/js/loginRegistracija.js"></script>
        </div>
        
      </div><!-- tab-content -->
      		  
</div> <!-- /form --> 
                </div>
<?php } ?>

                

                <div class="single-sidebar-widget popular-post-widget">
                  <h4 class="single-sidebar-widget__title">Popular Post</h4>
                  <div class="popular-post-list">
				  <?php 
				  for($i=0;$i<count($topTri);$i++){
				  ?>
                    <div class="single-post-list">
                      <div class="thumb">
                        <img class="card-img rounded-0" src="app/assets/img/<?= $topTri[$i]->Putanja; ?>" alt="<?= $topTri[$i]->NazivSlike; ?>">
                        <ul class="thumb-info">
                          <li><a href="#"><?= $topTri[$i]->Ime; ?></a></li>
                          <li><a href="#"><i class="fa fa-eye"></i><?= $topTri[$i]->BrojPregleda; ?></a></li>
                        </ul>
                      </div>
                      <div class="details mt-20">
                        <a href="index.php?page=SingePage&id=<?= $topTri[$i]->IdPost; ?>">
                          <h6><?= $topTri[$i]->NazivPosta; ?></h6>
                        </a>
                      </div>
                    </div>
					<?php 
					}
					?>
                    
                  </div>
                </div>

                  
                </div>
              </div>
            </div>
          <!-- End Blog Post Siddebar -->
        </div>
    </section>
    <!--================ End Blog Post Area =================-->
  </main>
