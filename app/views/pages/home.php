 <?php
         // var_dump($kategorije);
		 // var_dump($title);
		//var_dump($rez);
		//var_dump($vesti);
		
        ?>
 <main class="site-main">
    <!--================Hero Banner start =================-->  
    <section class="mb-30px">
      <div class="container">
        <div class="hero-banner">
          <div class="hero-banner__content">
            <h3 style="color:black">Vesnik</h3>
            <h1 style="color:black">Najzanimljivije i najnovije vesti!</h1>
            
          </div>
        </div>
      </div>
    </section>
    <!--================Hero Banner end =================-->  

   
    <!--================ Start Blog Post Area =================-->
    <section class="blog-post-area section-margin mt-4">
      <div class="container">
        <div class="row">
          <div class="col-lg-8">
			<div class="prikaz">

			</div>
           
            <div class="row">
              <div class="col-lg-12">
                  <nav class="blog-pagination justify-content-center d-flex">
                      <ul class="pagination">
                          
                      </ul>
                  </nav>
              </div>
            </div>
          </div>

          <!-- Start Blog Post Siddebar -->
		
          <div class="col-lg-4 sidebar-widgets">
              <div class="widget-wrap">
                <div class="single-sidebar-widget newsletter-widget"><div id="greska"></div>
				<?php if(isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"][0]->IdUl=="2"){
				?>
				<h1>Ime i prezime : <?php echo $_SESSION["korisnik"][0]->Ime . " " . $_SESSION["korisnik"][0]->Prezime; ?><h1>
			<h2>EMAIL: <?php echo $_SESSION["korisnik"][0]->Email; ?></h2>
			<form action="index.php?page=logout" method="post">
               </br> <input type="submit" name="odjava" value="ODJAVI SE" class="btn btn-primary "></div>
             </form>

				<?php
				}elseif(isset($_SESSION["korisnik"]) AND $_SESSION["korisnik"][0]->IdUl=="1"){
				echo "ZDRAVO ADMINE!<br/><a href='index.php?page=admin'>Admin panel</a>";
				?> 
				<form action="index.php?page=logout" method="post">
               </br> <input type="submit" name="odjava" value="ODJAVI SE" class="btn btn-primary "></div>
             </form>
				<?php
				}else{
				?>
                 <div class="form">
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Registracija</a></li>
        <li class="tab"><a href="#login">Logovanje</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1>Registruj se besplatno</h1>
          
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Ime<span class="req">*</span>
              </label>
              <input type="text" required autocomplete="off" id="ime"/>
            </div>
        
            <div class="field-wrap">
              <label>
                Prezime<span class="req">*</span>
              </label>
              <input type="text"required autocomplete="off" id="prezime"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" id="emailsignup"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id="passwordsignup"/>
          </div>
          
		  <div class="field-wrap">
            <label>
              Ponovi Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id="passwordsignup_confirm"/>
          </div>

          <button type="submit" class="button button-block" id="registracija"/>Registruj se</button>
          
          </form>

        </div>
        
        <div id="login">   
          <h1>Dobrodosli nazad!</h1>
          
          <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post">
		
            <div class="field-wrap">
            <label>
              Email<span class="req">*</span>
            </label>
            <input type="email"required autocomplete="off" id="username"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password"required autocomplete="off" id="password"/>
          </div>
          
        
          
          <button class="button button-block" id="login"/>Uloguj se</button>
          
          </form>
		 <script src="app/assets/js/loginRegistracija.js"></script>
        </div>
        
      </div><!-- tab-content -->
      		  
</div> <!-- /form --> 
                </div>
<?php } ?>

                <div class="single-sidebar-widget post-category-widget">
                  <h4 class="single-sidebar-widget__title">Catgory</h4>
                  <ul class="cat-list mt-20">
				  <li class="prikaziSve">
					<a href="#" class="d-flex justify-content-between" >
                        <p>PRIKAZI SVE VESTI</p>
                      </a></li>
					<?php 
					for($i=0;$i<count($kategorije);$i++){
					?><li class="kategorija" data-id="<?= $kategorije[$i]->Naziv;?>">
					<a href="#" class="d-flex justify-content-between" >
                        <p><?= $kategorije[$i]->Naziv; ?></p>
                      </a></li>
					<?php
					};
					?>
                   
                  </ul>
                </div>

                <div class="single-sidebar-widget popular-post-widget">
                  <h4 class="single-sidebar-widget__title">Popular Post</h4>
                  <div class="popular-post-list">
				  <?php 
				  for($i=0;$i<count($topTri);$i++){
				  ?>
                    <div class="single-post-list">
                      <div class="thumb">
                        <img class="card-img rounded-0" src="app/assets/img/<?= $topTri[$i]->Putanja; ?>" alt="<?= $topTri[$i]->NazivSlike; ?>">
                        <ul class="thumb-info">
                          <li><a href="#"><?= $topTri[$i]->Ime; ?></a></li>
                          <li><a href="#"><i class="fa fa-eye"></i><?= $topTri[$i]->BrojPregleda; ?></a></li>
                        </ul>
                      </div>
                      <div class="details mt-20">
                        <a href="index.php?page=SingePage&id=<?= $topTri[$i]->IdPost; ?>">
                          <h6><?= $topTri[$i]->NazivPosta; ?></h6>
                        </a>
                      </div>
                    </div>
					<?php 
					}
					?>
                    
                  </div>
                </div>

                  
                </div>
              </div>
            </div>
          <!-- End Blog Post Siddebar -->
        </div>
    </section>
    <!--================ End Blog Post Area =================-->
  </main>
