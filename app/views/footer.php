<!--================ Start Footer Area =================-->
  <footer class="footer-area section-padding">
    <div class="container">
      <div class="row">
        <div class="col-lg-3  col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>Menu</h6>
            <ul class="nav navbar-nav menu_nav justify-content-center">
			<?php 
			for($i=0;$i<count($menu);$i++){
			if($menu[$i]->link=="home"){
			?>
			<li class="nav-item active"><a class="nav-link" href="index.php"><?= $menu[$i]->naziv; ?></a></li> 
			<?php
			}else{
			?>
			<li class="nav-item active"><a class="nav-link" href="index.php?page=<?= $menu[$i]->link; ?>"><?= $menu[$i]->naziv; ?></a></li> 
			<?php
			}}	
			?>
             <li class="nav-item active"><a class="nav-link" href="https://www.linkedin.com/in/david-dujakovi%C4%87-2759671a2/">Autor</a></li>
	          <li class="nav-item active"><a class="nav-link" href="Dokumentacija.pdf">Dokumentacija</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-4  col-md-6 col-sm-6">
          
        </div>
        <div class="col-lg-3  col-md-6 col-sm-6">
          
        </div>
        <div class="col-lg-2 col-md-6 col-sm-6">
          <div class="single-footer-widget">
            <h6>Follow Us</h6>
            <p>Let us be social</p>
            <div class="footer-social d-flex align-items-center">
              <a href="#">
                <i class="fab fa-facebook-f"></i>
              </a>
              <a href="#">
                <i class="fab fa-twitter"></i>
              </a>
              <a href="#">
                <i class="fab fa-dribbble"></i>
              </a>
              <a href="#">
                <i class="fab fa-behance"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom d-flex justify-content-center align-items-center flex-wrap">
        <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
      </div>
    </div>
  </footer>
  <!--================ End Footer Area =================-->
  <script src="app/assets/js/jquery.min.js"></script>
  <script src="app/assets/vendors/jquery/jquery-3.2.1.min.js"></script>
 <script src"https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="app/assets/vendors/bootstrap/bootstrap.bundle.min.js"></script>
  <script src="app/assets/vendors/owl-carousel/owl.carousel.min.js"></script>
  <script src="app/assets/js/jquery.ajaxchimp.min.js"></script>
  <script src="app/assets/js/mail-script.js"></script>
  <script src="app/assets/js/main.js"></script>
   
	 <!--  <script src="app/assets/js/paginacija.js"></script>
	   <script src="app/assets/js/forma.js"></script>-->
	    <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="contact"){
   ?>
      <script src="app/assets/js/kontakt.js"></script>
	  <script src="app/assets/js/contact.js"></script>
   <?php
   }
   ?>
      <?php 
   if(!isset($_GET["page"])){
   ?>
      <script src="app/assets/js/paginacija.js"></script>
	   <script src="app/assets/js/forma.js"></script>
	   
   <?php
   }
   ?>
    <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="about"){
   ?>
   <script src="app/assets/js/forma.js"></script>
     
	
   <?php
   }
   ?><?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="SinglePage"){
   ?>
   <script src="app/assets/js/forma.js"></script>
    
	  <script src="app/assets/js/komentari.js"></script>
   <?php
   }
   ?>
   <?php 
   if(isset($_GET["page"]) AND $_GET["page"]=="admin"){
   ?>
   
       <script src="app/assets/js/admin.js"></script>
	   <script src="app/assets/js/forma.js"></script>
	
   <?php
   }
   ?>
</body>
</html>