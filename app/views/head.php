<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo $title ?></title>
	<link rel="icon" href="app/assets/img/Fevicon.png" type="image/png">

  <link rel="stylesheet" href="app/assets/vendors/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="app/assets/vendors/fontawesome/css/all.min.css">
  <link rel="stylesheet" href="app/assets/vendors/themify-icons/themify-icons.css">
  <link rel="stylesheet" href="app/assets/vendors/linericon/style.css">
  <link rel="stylesheet" href="app/assets/vendors/owl-carousel/owl.theme.default.min.css">
  <link rel="stylesheet" href="app/assets/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="app/assets/css/forma.css">
  <link rel="stylesheet" href="app/assets/css/style.css">
 
</head>