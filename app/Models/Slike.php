<?php

namespace App\Models;

class Slike {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   public function getImageWithParams($id){
        return $this->db->executeQueryWithParams("SELECT NazivSlike,Putanja FROM slikeposta sp INNER JOIN postovi p ON sp.IdPosta=p.IdPost WHERE sp.IdPosta=?",[$id]);
   }
}