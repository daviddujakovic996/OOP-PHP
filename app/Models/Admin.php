<?php

namespace App\Models;

class Admin {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   
	public function getVest($id){
		return $this->db->executeQueryWithParams("SELECT p.DetaljnoOPostu,p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,kat.IdKategorije,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						WHERE p.IdPost=? GROUP BY p.IdPost",[$id]);

	}

	public function izmeniVest($idPost,$naziv,$opis,$kat){
		return $this->db->insertUserQuery("UPDATE postovi SET NazivPosta=? , Opis=? ,IdKat=(SELECT IdKategorije FROM kategorije WHERE IdKategorije=?) WHERE IdPost=?",[$naziv,$opis,$kat,$idPost]);

	}
	public function getAll(){
	$prvi=0;
	$drugi=4;
		return $this->db->executeQuery("SELECT p.DetaljnoOPostu,p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,kat.IdKategorije,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						GROUP BY p.IdPost ORDER BY p.IdPost DESC LIMIT $prvi,$drugi");

	}

	public function unesiVest($naziv,$opis,$detaljnoOPostu,$kategorija){
		return $this->db->insertUserQuery("INSERT INTO postovi (NazivPosta,Opis,DetaljnoOPostu,IdAutor,IdKat) VALUES (?,?,?,(SELECT IdKorisnik FROM korisnici WHERE IdKorisnik='2'),(SELECT IdKategorije FROM kategorije WHERE IdKategorije=?))",[$naziv,$opis,$detaljnoOPostu,$kategorija]);
		
	}
	public function unesiSliku($fileName){
		return $this->db->insertUserQuery("INSERT INTO slikeposta (Putanja,NazivSlike,IdPosta) VALUES (?,(SELECT NazivPosta FROM postovi ORDER BY IdPost DESC LIMIT 1),(SELECT IdPost FROM postovi ORDER BY IdPost DESC LIMIT 1))",[$fileName]);
							
	}
	public function obrisiVest($id){
		
		return $this->db->insertUserQuery("DELETE FROM postovi WHERE IdPost=?",[$id]);
	}

	public function obrisiKorisnika($idKorisnik){
		return $this->db->insertUserQuery("DELETE FROM korisnici WHERE IdKorisnik=?",[$idKorisnik]);
	}
	public function getAllUser(){
		return $this->db->executeQuery("SELECT IdKorisnik,Ime,Prezime,Email,IdUl FROM korisnici");
	}
}