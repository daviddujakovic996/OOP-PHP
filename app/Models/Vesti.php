<?php

namespace App\Models;

class Vesti {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   public function getFour(){
		$prvi=0;
		$drugi=4;

        return $this->db->executeQuery("SELECT p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						GROUP BY p.IdPost ORDER BY p.IdPost DESC LIMIT $prvi,$drugi");
   }
   public function getAll(){				 
				return $this->db->executeQueryCount("SELECT * FROM postovi")->rowCount();
				//return $rez->rowCount();
   }

   public function getFourWithPagin($prvi,$drugi){
		return $this->db->executeQuery("SELECT p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						GROUP BY p.IdPost ORDER BY p.IdPost DESC LIMIT $prvi,$drugi");
   }
	public function topTri(){
		return $this->db->executeQuery("SELECT p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta GROUP BY p.IdPost ORDER BY p.BrojPregleda DESC LIMIT 3");
	}
	public function getVest($id){
		return $this->db->executeQueryWithParams("SELECT p.DetaljnoOPostu,p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						WHERE p.IdPost=? GROUP BY p.IdPost",[$id]);

	}
	public function podesibp($broj,$id){
		$uIntBrojPregleda=settype($broj,"integer");
		$broj++;
		return $this->db->insertUserQuery("UPDATE postovi SET BrojPregleda = ? WHERE IdPost = ?",[$broj,$id]);
 				
	}
}