<?php

namespace App\Models;

class Menu {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   public function getAll(){
        return $this->db->executeQuery("SELECT * FROM menu");
   }
}