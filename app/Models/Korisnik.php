<?php

namespace App\Models;

class Korisnik {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   public function getUser($email,$pass){
        return $this->db->executeQueryWithParams("SELECT IdKorisnik,Ime,Prezime,Email,IdUl FROM korisnici WHERE Email=? AND Sifra=?",[$email,$pass]);
   }
   public function insertUser($ime,$prezime,$email,$sifra){
   	   $upit="INSERT INTO korisnici (Ime, Prezime, Email, Sifra) VALUES (?, ?, ?, ?)";
        try{

			return $this->db->InsertUserQuery($upit,[$ime,$prezime,$email,$sifra]);

            http_response_code(201);
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
   }
   public function getAll(){
        return $this->db->executeQuery("SELECT IdKorisnik,Ime,Prezime,Email,IdUl FROM korisnici");
   }
}