<?php

namespace App\Models;

class Komentar {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   public function insertUser($naslov,$komentar,$korisnik,$vest){
	  
	 
   	   $upit="INSERT INTO komentari (Naslov, Komentar, IdPost, IdKor) VALUES (?, ?, (SELECT IdPost FROM postovi WHERE IdPost=?), (SELECT IdKorisnik FROM korisnici WHERE IdKorisnik=?))";
        try{

			return $this->db->InsertUserQuery($upit,[$naslov,$komentar,$vest,$korisnik]);

            http_response_code(201);
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
   }

   public function dohvatiKomentare($vest){
		 $upit="SELECT k.IdKomentar,k.Naslov,k.Komentar,kor.Ime,kor.Prezime FROM komentari k INNER JOIN korisnici kor ON k.IdKor=kor.IdKorisnik WHERE k.IdPost=? ORDER BY k.IdKomentar ASC";
        try{

			return $this->db->executeQueryWithParams($upit,[$vest]);

            http_response_code(201);
        }catch(PDOException $e){
			
            echo "Greska ".$e->getMessage();
        }
   }
}