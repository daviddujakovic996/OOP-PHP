<?php

namespace App\Models;

class Kategorije {
    private $db;

    public function __construct(DB $db){
        $this->db = $db;
    }

   public function getAll(){
        return $this->db->executeQuery("SELECT * FROM kategorije");
   }
   public function getKategorije($kat){
        return $this->db->executeQueryWithParams("SELECT p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						WHERE kat.Naziv=?
						GROUP BY p.IdPost ORDER BY p.IdPost DESC",[$kat]);
   }
   public function prebroj($kat){
        return $this->db->executeQueryCountWithParams("SELECT p.DatumObjave,p.IdPost,p.NazivPosta,k.Ime,p.BrojPregleda,kat.Naziv,sp.Putanja,sp.NazivSlike,p.Opis FROM postovi p 
						INNER JOIN korisnici k ON p.IdAutor=k.IdKorisnik INNER JOIN kategorije kat ON p.idKat=kat.IdKategorije INNER JOIN slikeposta sp ON p.IdPost=sp.IdPosta 
						WHERE kat.Naziv=?
						GROUP BY p.IdPost ORDER BY p.IdPost DESC",[$kat]);
   }
}