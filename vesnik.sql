-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2020 at 12:15 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vesnik`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategorije`
--

CREATE TABLE `kategorije` (
  `IdKategorije` int(10) NOT NULL,
  `Naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kategorije`
--

INSERT INTO `kategorije` (`IdKategorije`, `Naziv`) VALUES
(1, 'Fudbal'),
(2, 'Košarka'),
(3, 'Tenis'),
(4, 'Hokej'),
(5, 'Rukomet'),
(6, 'Odbojka'),
(7, 'Bejzbol'),
(8, 'Am. Fudbal'),
(9, 'Badminton'),
(10, 'Biciklizam'),
(11, 'Boks'),
(12, 'Esportovi'),
(13, 'Futsal'),
(14, 'Golf'),
(15, 'Kriket'),
(16, 'MMA'),
(17, 'Moto Sportovi'),
(18, 'Fud. na pijesku'),
(19, 'Odb. na pijesku'),
(20, 'Pikado'),
(21, 'Rugby'),
(22, 'Stoni Tenis'),
(23, 'Trke Konja'),
(24, 'Vaterpolo');

-- --------------------------------------------------------

--
-- Table structure for table `komentari`
--

CREATE TABLE `komentari` (
  `IdKomentar` int(255) NOT NULL,
  `Naslov` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Komentar` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `IdPost` int(255) DEFAULT NULL,
  `IdKor` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `IdKorisnik` int(100) NOT NULL,
  `Ime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Prezime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Sifra` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `IdUl` int(10) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`IdKorisnik`, `Ime`, `Prezime`, `Email`, `Sifra`, `IdUl`) VALUES
(1, 'David', 'Dujakovic', 'david.dujakovic.342.17@ict.edu.rs', '172522ec1028ab781d9dfd17eaca4427', 2),
(2, 'Admin', 'Administrirovic', 'admin@hotmail.com', 'e64b78fc3bc91bcbc7dc232ba8ec59e0', 1),
(3, 'David', 'Dujakovic', 'david_dujakovic_96@hotmail.com', '97621c74f2930192a198b5905aa8e764', 2),
(4, 'Pera', 'Peric', 'pera.peric@hotmail.com', '98a4a0d0dac6e479ee08b363d8ed11ad', 2),
(5, 'Mika', 'Mikic', 'mika.mikic@hotmail.com', '1d70590b8d6afd0a748a662fbd468db6', 2),
(6, 'Pera', 'Peric', 'nesto.nesto@gmail.com', '98a4a0d0dac6e479ee08b363d8ed11ad', 2),
(8, 'Mika', 'Mikic', 'mika@gmail.com', '1d70590b8d6afd0a748a662fbd468db6', 2),
(9, 'Mika', 'Mikic', 'mikaaa@gmail.com', '1d70590b8d6afd0a748a662fbd468db6', 2),
(10, 'Ana', 'Anic', 'ana@gmail.com', 'd1659f8a4945d81e506e5fee09867042', 2),
(11, 'David', 'Davidic', 'mojnovi@hotmail.com', 'd64b4f5236ea108c6a761f5dad8aa09d', 2),
(13, 'David', 'Davidic', 'mojnovi@nesto.com', 'd64b4f5236ea108c6a761f5dad8aa09d', 2),
(15, 'David', 'Davidic', 'blabla@blabla.com', 'd64b4f5236ea108c6a761f5dad8aa09d', 2),
(16, 'Dadasddads', 'Cascacsa', 'acacc@ascasac.com', '294fdce89642d42592880bbdb4b361f9', 2),
(17, 'Asacaccxcz', 'Axzvdv', 'konj@konj.com', '84862b699f649b789df9c23011051209', 2),
(19, 'Asacaccxcz', 'Axzvdv', 'konja@konj.com', 'f2053bddf01406f3bc83a741472cbdc8', 2),
(20, 'Data', 'Datic', 'data@data.com', '5ee5a59fe5e6ee8b955cc60a8646d47b', 2),
(21, 'Ajdee', 'Ajdeee', 'ajde@hotmail.com', 'abc20ed5c49dfd834ec21d208d1fd72b', 2),
(22, 'Nema', 'Nemas', 'nema@nema.com', '06801d44b88c2334e87c5441cdbbf2d1', 2),
(23, 'Hsddsjd', 'Aaaaaaxx', 'kome@jsjs.com', '5b076be2e242511299fac39bdbbf143b', 2),
(24, 'Dobar', 'Dan', 'dobar@hotmail.com', '191dc858f6d7bad26178f752247b36a3', 2),
(25, 'Nes', 'Nest', 'nes@hot.com', 'fa25953be1c76ade36fc0968a17ea626', 2),
(26, 'Prov', 'Prove', 'provera@hotmail.com', '33194baecb65cceefeb06c2ee7a43236', 2),
(27, 'Adasd', 'Jlslds', 'jama@jam.com', 'a0fca46154d85f9b2f337e1b9ad0505d', 2),
(28, 'David', 'Davidas', 'davidas@davidas.com', '5bb2f1bf13678092167c189ea7203356', 2),
(30, 'Sesija', 'Sesijic', 'sesija@hotmail.com', '6c90aacc285d6505ad07cfab0724a7c7', 2),
(31, 'Pera', 'Peric', 'pera@pericc.com', 'bb21a26301198c657a25e72f7b0f5149', 2),
(32, 'Uros', 'Vasiljevic', 'uros@gmail.com', '202cb962ac59075b964b07152d234b70', 2),
(33, 'Dasdas', 'Dasda', 'Dsasdadas@asdasd.com', '202cb962ac59075b964b07152d234b70', 2),
(34, 'Dasdsa', 'Dadsad', 'sad@mail.com', '202cb962ac59075b964b07152d234b70', 2),
(37, 'Dsa', 'Dad', 'adsas@dasd.com', '202cb962ac59075b964b07152d234b70', 2),
(38, 'Ass', 'Ass', 'as@gmail.com', 'c20ad4d76fe97759aa27a0c99bff6710', 2),
(39, 'Sad', 'Asd', 'sad@gmail.com', 'c20ad4d76fe97759aa27a0c99bff6710', 2),
(40, 'Mmm', 'Mmm', 'mm@gmail.com', 'c20ad4d76fe97759aa27a0c99bff6710', 2),
(54, 'Aasdsds', 'Dada', 'asdasda@asdsadadas.com', '202cb962ac59075b964b07152d234b70', 2);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(11) NOT NULL,
  `naziv` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`idMenu`, `naziv`, `link`) VALUES
(1, 'Početna', 'home'),
(2, 'O nama', 'about'),
(5, 'Kontakt', 'contact');

-- --------------------------------------------------------

--
-- Table structure for table `postovi`
--

CREATE TABLE `postovi` (
  `IdPost` int(255) NOT NULL,
  `NazivPosta` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Opis` varchar(1200) COLLATE utf8_unicode_ci NOT NULL,
  `DetaljnoOPostu` text COLLATE utf8_unicode_ci NOT NULL,
  `DatumObjave` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `BrojPregleda` int(150) DEFAULT '0',
  `IdAutor` int(10) NOT NULL,
  `idKat` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `postovi`
--

INSERT INTO `postovi` (`IdPost`, `NazivPosta`, `Opis`, `DetaljnoOPostu`, `DatumObjave`, `BrojPregleda`, `IdAutor`, `idKat`) VALUES
(37, '\r\nNatho raduje Grobare: U Partizanu ću ostati duže nego što iko očekuje', '\"Klub je tačno onakav kakav sam zamišljao za ovu fazu karijere\", kaže lider crno-belih', 'Kad navijači, a onda i deo stručnog štaba, daju fudbaleru nadimak Tatko, jasno je onda o kakvom se znalcu na terenu, a i van njega, radi. A, Bibars Natho je zaista “tata” igre Partizana i to od momenta čim je stao na zeleni tepih stadiona u Humskoj.\r\n\r\nNe pamti se kad se desilo poslednji put da jedan igrač u Partizanu za tako kratko vreme, za jednu utakmicu, potpuno ovlada terenom, timom, osvoji navijače i bez nekih suvišnih poteza ili ičeg na silu, postane gazda na terenu, lider, uzor svima u ekipi.\r\n\r\nA, kapiten izraelske reprezentacije, na radost navijača, najavljuje da nema nameru da skoro ode iz kluba. Naprotiv, sviđa mu se Partizan, sviđa mu se Beograd i u razgovoru u podkastu za izraleski Gol, poručio je da će ostati tu još dugo.\r\n“Klub je tačno onakav kakav sam zamišljao za ovu fazu karijere i mislim da je ovo mesto na kojem ću ostati duže nego što bilo ko očekuje”, rekao je Natho.\r\n\r\nIspričao je Bibars i poznatu priču kako je došao u Humsku.\r\n“Sa Zoranom Tošićem, sa kojim sada igram u Partizanu, bio sam saigrač prethodno u CSKA iz Moskve. Video je na instagramu da treniram sam, pozvao me i pitao šta se dešava, rekao sam mu da sam raskinuo ugovor sa Olimpijakosom. Pitao me da li bih došao u Partizan i evo nas”.\r\n\r\nDopada se Nathu i Beograd, a upravo je izbor grada bio jedan od ključnih faktora kada je vagao letos između više ponuda.\r\n“Grad je veoma lep. U jednu ruku nije prevelik, kao neka mesta na kojima sam ranije bio, ali u drugu – ima sve. Ljudi su sjajni. Imaju topao karakter kao Izraelci, ali su pomalo i kao Rusi, što je za mene najbolja kombinacija”.\r\n\r\nNaravno, kakav bi to bio razgovor o srpskom fudbalu, a da se na pomene – večiti derbi.\r\n“Sve vreme samo slušaš priču o derbiju. Derbi, derbi, derbi... I sve ti postane jasno zašto je tako kada izađeš na teren. To je neka drugačija vrsta uzbuđenja, drugačiji vajb, ništa sa čim bi mogao da uporedim. Stvarno je derbi nešto posebno”.', '2020-02-16 13:28:57', 0, 2, 1),
(38, 'To nije nepravda, to je krađa na pravdi Boga! Svi su videli da je Aron Gordon bolji... Osim žirija ', 'Kontroverze u Čikagu, Badi Hild trijumfovao u brzom šutiranju trojki', 'Nepravda! Deriku Džonsu Džunioru trofej, Aronu Gordonu naklonost publike i opšte mišljenje da je zaslužio nagradu. As Majamija pobedio je u takmičenju zakucavanja na Ol staru u Čikagu, koje se već sad  smatra kontroverznim zbog odluka žirija, imunog na potez krila Orlanda iz dodatne serije, kad su svi videli da je izveo spektakularniju akciju od rivala.\r\n\r\nPosle četiri pokušaja Džons i Gordon su bili izjednačeni po poenima, zbog čega su morali u baraž. A u njemu je Džons za let skoro sa linije slobodnih bacanja nagrađen sa 48 poena. Usledio je odgovor Gordona, konsultacije sa legendarnim Šekilom O’Nilom, pomoć u vidu Takoa Fola iz Bostona, inače visokog 226 centimetara, spektakularan odraz i – BUM!\r\n\r\nA onda šok, jer je Gordon za taj potez dobio samo 47 od mogućih 50 poena (maksimum su imali u prve četiri serije) i ostao je kratak, jer je Džons nagrađen sa 48 koji sekund ranije.\r\n„Imao sam četiri pedesetice. Ma, svih pet je trebalo da bude. Kraj priče. Ako imate četiri maksimalne ocene u takmičenju zakucavanja trebalo bi da je gotovo. Eto, ne znam. Ko uopšte vodi šou“, zapitao se Gordon, nezadovoljan što mu je izmakla nagrada.\r\n\r\nKažu da o ukusima ne bi trebalo raspravljati, ali ono što je Aron uradio zaista je delovao ubedljivije od Džonsa. Pogotovo što je i ranije, pre tog baraža, izveo fenomenalan potez (u četvrtoj seriji) i počeo da slavi, uveren da mu ovoga puta neće izmaći pehar, kao 2016. kad je na pravdi Boga izgubio od Zeka Lavina.\r\n„Osećam da je trebalo da imam dva pehara u kolekciji“, jadao se Gordon i pojasnio koliko ga nepravda boli. „Mislim da više neću učestvovati na ovom takmičenju. Smatram sebe jedni od najboljih, ako ne i najboljim u zakucavanjima na svetu. Uradio sam sve što je potrebno da pobedim“.\r\n\r\nRezignirani as Orlanda na kraju je, ipak, morao da stisne ruku rivalu, koji je dobio najlepši poklon za 23. rođendan.\r\n„Kačio je Gordon Takoovu glavu, tako da sudije nisu mogle da mu daju 50. Očekivao sam da ga nagrade sa 48 poena, pa da idemo u još jednu seriju“, jasno je i Džonsu da je moglo drugačije da se završi.\r\n\r\nTek da pomenemo, žiri koji ocenjuje takmičara u zakucavanju sastoji se od pet članova. U njemu su ove godine bili nekadašnji NBA asovi, Dvejn Vejd i Skoti Pipen, potom Kadens Parker, sadašnja košarkašica Los Anđeles Sparksa, kao i reper Komon i glumac Čedvik Bosman. Nije baš da su poslednja dvojica kompetentni, ali...\r\n\r\nU takmičenju u šutiranju trojki slavio je Badi Hild. As Sakramenta je u finalnoj rundi porazio Devina Bukera (27:26), pri čemu je ubacio sedam od završnih osam pokušaja. Hild je tako postao prvi košarkaš Kingsa posle Predraga Stojakovića 2003. koji je trijumfovao u ovoj kategoriji.\r\n\r\nPrethodno, u takmičenju veštine Bem Adebajo iz Majamija savladao je Indijaninog Domantasa Sabonisa.', '2020-02-16 13:32:52', 0, 2, 2),
(39, '\r\nSVA FINALA VEČITIH U 21. VEKU: Od prekida u Kragujevcu do Lendejlovih bacanja za pobedu', 'Čeka nas sedmi okršaj Partizana i Crvene zvezde u Kupu Radivoja Koraća od 2003. godine, od kada takmičenje nosi ovo ime', 'Večiti derbi, pa još u finalu Kupa Radivoja Koraća. Mnogi bi rekli da je to redovna stvar kada je u pitanju nacionalni kup, ali brojke ipak pokazuju da će ovo biti tek sedmi put u 21. veku (na 18 ukupno održanih turnira) da će se Crvena zvezda i Partizan direktno obračunati za Žućkovu levicu.\r\n\r\nUmeli su planove crveno-belih i crno-belih kroz godine da pokvare pre svega Mega i FMP, ali oni malo stariji sećaju se da je neretko do finala stizao i vršački Hemofarm, koji je u međuvremenu doživeo tužnu sudbinu, a Vršac kao njegov naslednik pokušava da vrati staru košarkašku slavu ovom gradu.\r\n\r\nBilo je svega i svačega tokom minulih godina, počev od pobeda u poslednjim sekundama utakmice, pa sve do onog neslavnog finala u kragujevačkom Jezeru, koje je na kraju razvučeno na dva dana zbog velikih incidenata na tribinama i prekida utakmice, kada su morale da budu ispražnjene kompletne tribine.\r\n\r\nAko ćemo da se vodimo isključivo brojkama, Partizan je taj koji je u Kupu Radivoja Koraća do sada ostvario za nijansu bolje rezultate od Crvene zvezde. Crno-beli su uspeli da se domognu sedam trofeja i da još pet puta budu finalisti, dok je klubu sa Malog Kalemegdana šest puta pošlo od ruke da podigne Žućkovu levicu, a da još četiri puta izgubi u finalu. Zanimljvo, sva četiri puta su košarkaši Zvezde u završnom meču gubili od Partizana.\r\n\r\nI u međusobnom skoru, kada obuhvatimo samo finalni meč, Partizan ima prednost u odnosu na Crvenu zvezdu. Naime, šest puta su se ljuti rivali do sada sastajali u završnici, a četiri puta je tim u crno-belim dresovima bio taj koji se radovao, naspram Zvezdina dva trijumfa.', '2020-02-16 18:20:10', 0, 2, 2),
(40, 'Novi teniski uzlet: Srbija sa petoricom igrača u top 50', 'Naša zemlja u društvu Španije i Francuske', 'Pre nešto manje od deceniju činilo se da je srpski tenis dostigao vrhunac. Naši teniseri “flertovali“ su sa top deset sve češće i češće, Novak Đoković je 2011. godinu prvi put završio na prvom mestu, sredinom iste Viktor Troicki je bio na 12. poziciji, Janko Tipsarević je godinu dana kasnije stigao do Top 10 (osmo mesto), dok je Nenad Zimonjić odavno bio u vrhu dubla.\r\n\r\nIzgleda da je srpski tenis u 2020. doživeo novi uzlet, jer će od sutra Srbija imati čak petoricu igrača među 50 najbolje rangiranih tenisera sveta!\r\n\r\nNije Đokoviću dugo trebalo da se vrati na teniski tron. Nakon što je 2019. završio na drugoj poziciji, Novak se osvajanjem Australijan opena ekspresno vratio na vrh na kojem će sutra početi 278 nedelju u karijeri. Razliku u odnosu na rivale bi dodatno mogao da poveća već na turniru u Dubaiju, koji je prvi reket sveta osvajao četiri puta.\r\n\r\nNa novoj listi, koja će izaći u ponedeljak, drugi reket Srbije biće i dalje Dušan Lajović. Znatno je napredovao Filip Krajinović, koji je ove nedelje zabeležio skok od šest mesta i od sutra bi mogao da bude 33. Progresiju na ATP listi zaradio je odličnim igrama u Monpeljeu i Roterdamu, a oba puta ga je u polufinalu zaustavio Gael Monfis. Na kojoj će tačno poziciji osvanuti u ponedeljak, zavisi od ishoda današnjih finala u Njujorku i Buenos Ajresu.\r\n\r\nKrajinović je dobrim rezultatima preskočio Lasla Đerea, koji će u ponedeljak biti 35. ili 36. na planeti.\r\n\r\nDo članstva u 50 najboljih Miomir Kecmanović je stigao na turniru u Njujorku savladavši u četvrtfinalu Uga Umberta i sa 54. mesta preselio se na 49. poziciju, na kojoj se trenutno nalazi. Ipak, u narednom kolu poražen je od  Kajla Edmunda, pa će naš teniser, ukoliko Britanac u finalu trijumfuje protiv veterana Andreasa Sepija, narednu nedelju dočekati na 50. mestu.\r\n\r\nSrbija se pridružila Španiji (Rafael Nadal, Roberto Bautista Agut, Pablo Karenjo Busta, Alberto Ramos Vinjolas, Fernando Verdasko) i Francuskoj (Gael Monfis, Benoa Per, Adrijan Manarino, Ig Umber, Žo-Vilfred Conga) kao treća nacija sa petoricom igrača u top 50.', '2020-02-16 18:24:59', 0, 2, 3),
(41, 'Liverpul prodaje Grujića i traži 30.000.000 evra', 'Na Enfildu očekuju lepu zaradu', 'Bio je Marko Grujić prvo pojačanje Jirgena Klopa kada je Nemac seo za komande Liverpula, ali kako stvari stoje, neće srpski fudbaler dočekati i da zaigra u sastavu Crvenih.\r\n\r\nProlaze godina za godinom, a Grujić ide na pozajmicu za pozajmicom i stiče se utisak da i pored toga što igra odlično u Herti u poslednje dve sezone, na Enfild roudu ga ne vide u timu u skorijoj budućnosti. Zbog toga su čelnici kluba rešili da na leto prodaju srpskog fudbalera, koji ima ugovor do leta 2023. godine, pišu britanski mediji.\r\n\r\nProcenjeno je u Liverpulu da je Grujić značajno napredovao od dana kada je potpisao za Crvene, ali da i dalje ima dosta igrača u Klopovom timu koji su ispred njega. Zanimljivo je da se već zna i cena koju će prvak Evrope tražiti za srpskog fudbalera – 30.000.000 evra. I ukoliko “upali”, klub sa Enfilda će višestruko zaraditi od transfera, jer je Marka doveo iz Crvene zvezde pre četiri godine za 6.000.000 evra.\r\n\r\nOd tog dana Grujić je odigrao samo 14 mečeva za Liverpul, uglavnom u pripremnom periodu, a išao je na pozajmice prvo u Kardif, potom u Hertu, za koju nastupa od leta 2018. godine i u kojoj se etablirao kao jedan od vodećih igrača.\r\n\r\nBivši trener berlinskog kluba Pal Dardai rekao je jednom prilikom za Grujića da je “daleko najbolji vezista kojeg je ikada video u klubu”, a u Herti je bio 20 godina. Upravo će nemački klub biti prvi u redu za kupovinu srpskog fudbalera, mada nema dileme da će biti još zainteresovanih, kao što ih je bilo i letos, kada je Liverpul birao gde će Marka poslati na novu pozajmicu.', '2020-02-16 18:24:59', 0, 2, 1),
(42, '\r\nEspanjol vidi svetlo na kraju tunelu: Abelardov efekat je primetan', 'Klub iz Barselone odoleo u Sevilji', 'Bio je red da im krene. U prošlom kolu prva pobeda kod kuće u sezoni, a u ovom bod na vrućem gostovanju u Andaluziji. Dolazak Abelarda Fernandeza promenio je krvnu sliku manje poznatog tima iz Barselone, te Espanjol i te kako ima čemu da se nada u završnici sezone koja je slutila na apokalipsu. Plavo-beli su na otvaranju nedeljnog programa 24. runde La Lige odoleli u Sevilji i popravili šanse da jesen dočekaju u društvu najboljih španskih klubova.\r\n\r\nKakav je zaokret napravio nekadašnji reprezenatativac Crvene furije na klupi Espanjola kazuje i kretanje rezultata na stadionu „Ramon Sančez Pishuan“, pošto su gosti pokazali psihološku stabilnost, okrenuli rezultat u svoju korist posle zaostatka u ranoj fazi meča, ali nisu uspeli da sačuvaju prednost, pošto su završnicu dočekali brojčano neravnopravni – 2:2.\r\n\r\nRemi na terenu kandidata za plasman u Ligu šampiona i te kako se može smatrati pozitivnim učinkom za Abelardov tim, naročito ako se zna da je igrao bez najvrednijem zimskog pojačanja, povređenog napadača Raula de Tomasa. U njegovom odustvu zablistao je drugi novajlija iz prethodnog prelaznog toka, Adrijan Embarba, plaćen Rajo Valjekanu 10.000.000 evra. Desnokrilni vezista je pokazao da ume da bude opasan iz prekida, mudrim potezom iz slobodnog udarca doneo je izjednačenje, a Kinez Vu Lei, početkom nastavka i preokret, sedmim golom u sezoni u svim takmičenjima, a prvim otkako je pre mesec i po dana pogodio za remi sa gradskim rivalom Barselonom. Baš od toga meča, Espanjol je sa Abelardom na klupi osvojio devet od mogućih 18 bodova i postao kandidat da se spasi.', '2020-02-16 18:31:30', 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slikeposta`
--

CREATE TABLE `slikeposta` (
  `IdSlika` int(100) NOT NULL,
  `Putanja` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'blog4.png',
  `NazivSlike` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IdPosta` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slikeposta`
--

INSERT INTO `slikeposta` (`IdSlika`, `Putanja`, `NazivSlike`, `IdPosta`) VALUES
(44, '7a08873c7c4c6ddbfa19e305cabe0572e840f6ae.jpg', 'Natho', 37),
(45, '20190919npNatcho16.jpg', 'Natho', 37),
(46, '8f663ef8c09669049cec53b242172a752e3e3665.jpg', 'NBA', 38),
(47, '2020-02-16T035935Z_1593773856_NOCID_RTRMADP_3_NBA-ALL-STAR-SATURDAY-NIGHT.jpg', 'NBA', 38),
(48, 'ce9a5c18cdf355a4f135bcc5913e61805fea3d5b.jpg', 'Korac', 39),
(49, 'b13a0387ef10d37a3f3397427733dbce1933f26d.jpg', 'Srbija tenis', 40),
(50, '531ddec7711d21f084ddeda666c92bcf37392736.jpg', 'Grujic', 41),
(51, 'eb130a52757f3bfe11476f987093e2956606bd9d.jpg', 'espanjol', 42);

-- --------------------------------------------------------

--
-- Table structure for table `uloge`
--

CREATE TABLE `uloge` (
  `IdUloge` int(10) NOT NULL,
  `Naziv` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uloge`
--

INSERT INTO `uloge` (`IdUloge`, `Naziv`) VALUES
(1, 'Administrator'),
(2, 'Korisnik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategorije`
--
ALTER TABLE `kategorije`
  ADD PRIMARY KEY (`IdKategorije`);

--
-- Indexes for table `komentari`
--
ALTER TABLE `komentari`
  ADD PRIMARY KEY (`IdKomentar`),
  ADD KEY `IdKor` (`IdKor`),
  ADD KEY `IdPost` (`IdPost`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`IdKorisnik`),
  ADD UNIQUE KEY `Email` (`Email`),
  ADD KEY `korisnici_ibfk_1` (`IdUl`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`);

--
-- Indexes for table `postovi`
--
ALTER TABLE `postovi`
  ADD PRIMARY KEY (`IdPost`),
  ADD KEY `IdAutor` (`IdAutor`),
  ADD KEY `postovi_ibfk_2` (`idKat`);

--
-- Indexes for table `slikeposta`
--
ALTER TABLE `slikeposta`
  ADD PRIMARY KEY (`IdSlika`),
  ADD KEY `IdPosta` (`IdPosta`);

--
-- Indexes for table `uloge`
--
ALTER TABLE `uloge`
  ADD PRIMARY KEY (`IdUloge`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategorije`
--
ALTER TABLE `kategorije`
  MODIFY `IdKategorije` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `komentari`
--
ALTER TABLE `komentari`
  MODIFY `IdKomentar` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `IdKorisnik` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `postovi`
--
ALTER TABLE `postovi`
  MODIFY `IdPost` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `slikeposta`
--
ALTER TABLE `slikeposta`
  MODIFY `IdSlika` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `uloge`
--
ALTER TABLE `uloge`
  MODIFY `IdUloge` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `komentari`
--
ALTER TABLE `komentari`
  ADD CONSTRAINT `komentari_ibfk_1` FOREIGN KEY (`IdKor`) REFERENCES `korisnici` (`IdKorisnik`),
  ADD CONSTRAINT `komentari_ibfk_2` FOREIGN KEY (`IdPost`) REFERENCES `postovi` (`IdPost`);

--
-- Constraints for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD CONSTRAINT `korisnici_ibfk_1` FOREIGN KEY (`IdUl`) REFERENCES `uloge` (`IdUloge`) ON UPDATE CASCADE;

--
-- Constraints for table `postovi`
--
ALTER TABLE `postovi`
  ADD CONSTRAINT `postovi_ibfk_1` FOREIGN KEY (`IdAutor`) REFERENCES `korisnici` (`IdKorisnik`),
  ADD CONSTRAINT `postovi_ibfk_2` FOREIGN KEY (`idKat`) REFERENCES `kategorije` (`IdKategorije`) ON DELETE CASCADE;

--
-- Constraints for table `slikeposta`
--
ALTER TABLE `slikeposta`
  ADD CONSTRAINT `slikeposta_ibfk_1` FOREIGN KEY (`IdPosta`) REFERENCES `postovi` (`IdPost`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
