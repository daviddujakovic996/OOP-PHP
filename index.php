<?php
session_start();
require_once "app/config/autoload.php";
require_once "app/config/database.php";

use App\Models\DB;
use App\Controllers\PageController;
use App\Controllers\PaginacijaController;
use App\Controllers\LoginController;
use App\Controllers\LogoutController;
use App\Controllers\RegistrationController;
use App\Controllers\KategorijaController;
use App\Controllers\KontaktController;
use App\Controllers\KomentariController;
use App\Controllers\AdminController;

$db = new DB(SERVER, DATABASE, USERNAME, PASSWORD);

$pageController = new PageController($db);




if(isset($_GET['page'])){
	switch($_GET['page']){
       case "paginacija":    
		  $paginacijaController = new PaginacijaController($db);
          $paginacijaController->prikazi();		
          break;
		case "login":    
		  $loginController = new LoginController($db);
          $loginController->ulogujSe();		
          break;
		case "logout":
		  $logoutController = new LogoutController($db);
          $logoutController->logout();	
		  break;
		case "registracija":
		  $registrationController = new RegistrationController($db);
          $registrationController->registracija();	
		  break;
		case "kategorija":
		  $kategorijaController = new KategorijaController($db);
          $kategorijaController->prikaziKategorije();	
		  break;
		case "contact":
		  $pageController->contact();
		  $kontaktController = new KontaktController($db);
          $kontaktController->validirajKontakt();	
		  break;
		case "about":
		  $pageController->about();
		  break;
		case "SinglePage":
		  $pageController->singlePage($_GET);
		  	
		  break;
		case "komentari":
		  $komentariController = new KomentariController($db);
          $komentariController->unesiKomentar();	
		  break;
		case "admin":
			$pageController->admin();
			break;
		case "izmeniPostForma":
			$adminController = new AdminController($db);
			$adminController->formaIzmeni();
			break;
		case "izmeniPost":
			$adminController = new AdminController($db);
			$adminController->izmeniPost();
			break;
		case "unesiPost":
			$adminController = new AdminController($db);
			$adminController->unesiPost();
			break;
		case "obrisiPost":
			$adminController = new AdminController($db);
			$adminController->obrisiPost();
			break;
		case "obrisiKorisnika":
			$adminController = new AdminController($db);
			$adminController->obrisiKorisnika();
			break;
 }
} else {
    $pageController->home();	
	 
}

